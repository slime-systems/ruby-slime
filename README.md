# RubySlime
## What is it?
It is a collection of infrastructure utilities utilized by [Slime Systems](https://slime.systems/).

## Installation

Add this line to your application's Gemfile:

~~~ruby
gem 'ruby_slime'
~~~

And then execute:

    $ bundle install

## License
Currently, we use the library privately; we don't think it will gain any attention as we never explain anything to anyone. 
However, RubySlime is released under the [3-clause BSD License](LICENSE.md) anyway.
