# frozen_string_literal: true

require_relative 'lib/ruby_slime/version'

::Gem::Specification.new do |spec|
  spec.name = 'ruby_slime'
  spec.version = ::RubySlime::VERSION
  spec.author = 'Sarun Rattanasiri'
  spec.email = 'midnight_w@gmx.tw'
  spec.license = 'BSD-3-Clause'

  spec.summary = 'The ruby-type slime made by Slime Systems.'
  spec.homepage = 'https://slime.systems/'
  spec.required_ruby_version = '>= 3.0.0'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/slime-systems/ruby-slime'

  spec.files = [
    *::Dir['lib/**/*'],
    'README.md',
    'LICENSE.md',
  ]
  spec.require_paths = ['lib']

  spec.add_dependency 'activemodel'
  spec.add_dependency 'activesupport'
  spec.add_dependency 'dry-schema'
  spec.add_dependency 'json-canonicalization', '~> 1.0'
  spec.add_dependency 'jwe'
  spec.add_dependency 'jwt'
  spec.add_dependency 'mongoid', '>= 8.1.2' # bug: MONGOID-5632
  spec.add_dependency 'typhoeus'
  spec.add_dependency 'zeitwerk'
end
