# frozen_string_literal: true

require 'active_model'
require 'active_support'
require 'active_support/core_ext/hash/indifferent_access'
require 'active_support/core_ext/object/blank'
require 'active_support/core_ext/object/try'
require 'active_support/core_ext/time'
require 'forwardable'
require 'singleton'
require 'zeitwerk'

loader = ::Zeitwerk::Loader.for_gem
loader.inflector.inflect(
  'api_verifier' => 'APIVerifier',
  'json_stream' => 'JSONStream',
  'jwt_kit' => 'JWTKit',
)
::File.join(*[__dir__, 'ruby_slime'].compact).then do |base_path|
  %w[jwt_kit mongoid rake].flat_map do |autoload_module|
    [
      ::File.join(base_path, "#{autoload_module}.rb"),
      ::File.join(base_path, autoload_module),
    ]
  end
end.tap do |autoload_paths|
  loader.do_not_eager_load(*autoload_paths)
end
loader.setup

module RubySlime
end
