# frozen_string_literal: true

module RubySlime
  module Mongoid
    class State
      include ::Mongoid::Document
      include QuerySharding
      store_in collection: 'process_states'

      field :_s, as: :state, type: :hash, default: {}
      field :_np, as: :next_position, type: :integer, default: 1
      field :_lv, as: :lock_version, type: :integer, default: 0
      proc do
        ::BSON::ObjectId.new
      end.tap do |value_proc|
        field :_c, as: :creation_id, type: :object_id, default: value_proc
      end

      declare_index = proc do |namer|
        index(*namer.call({
          creation_id: 1,
        }))
      end

      IndexNamer.new(self).tap(&declare_index)
    end
  end
end
