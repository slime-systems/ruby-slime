# frozen_string_literal: true

module RubySlime
  module Mongoid
    module ModelHelpers
      def mongoid_upsert(scope, attributes)
        scoped = where(scope)
        create_error = nil
        3.times do
          result = scoped.update_all({ '$set': attributes })
          break result if 0 < result.matched_count
          raise create_error if create_error
          begin
            break create!({ **scope, **attributes })
          rescue ::Mongo::Error::OperationFailure => e
            raise e unless e.code == 11000
            create_error = e
          end
        end
      end

      def mongoid_indate(scope, attributes)
        create!({ **scope, **attributes })
      rescue ::Mongo::Error::OperationFailure => e
        raise e unless e.code == 11000
        where(scope).update_all({
          '$set': attributes,
        })
      end
    end
  end
end
