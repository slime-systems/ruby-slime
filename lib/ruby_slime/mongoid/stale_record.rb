# frozen_string_literal: true

module RubySlime
  module Mongoid
    class StaleRecord < ::StandardError
      include ConcurrentWriteDetected
    end
  end
end
