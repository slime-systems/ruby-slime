# frozen_string_literal: true

module RubySlime
  module Mongoid
    class GlobalCount
      include ::Mongoid::Document
      store_in collection: 'key_value'
      DATABASE_ID = {
        type: 'global-count',
      }.freeze
      private_constant(:DATABASE_ID)

      proc do
        (1 + Integer(::SecureRandom.hex(3), 16)) & 0x07ffff
      end.then do |default_value|
        field :_nv, as: :next_value, type: :integer, default: default_value
      end

      class << self
        def next_value
          loop do
            record = begin
              where({ _id: DATABASE_ID }).first_or_create!
            rescue ::Mongo::Error::OperationFailure => e
              next if e.code == 11000
              raise e
            end
            current_value = record.next_value
            result = where({
              _id: DATABASE_ID,
              next_value: current_value,
            }).update_all({
              '$inc': {
                next_value: 1,
              },
            })
            break current_value if 0 < result.modified_count
          end
        end

        def peek
          where({ _id: DATABASE_ID }).first
        end
      end
    end
  end
end
