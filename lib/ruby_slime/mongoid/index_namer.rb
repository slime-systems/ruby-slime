# frozen_string_literal: true

require 'base64'
require 'openssl'
require 'json/canonicalization'

module RubySlime
  module Mongoid
    class IndexNamer
      def initialize(document_klass)
        name = String(document_klass.collection_name)
        @hash_prototype = PREFIXED_DIGEST.dup.tap do |d|
          d << "#{name}::>"
        end
        @field_mapper = document_klass.method(:database_field_name)
        freeze
      end

      def call(config)
        hexdigest = @hash_prototype.dup.tap do |d|
          d << config.transform_keys(&@field_mapper).to_json_c14n
        end.hexdigest
        [config, {
          name: "index_#{hexdigest[0, 10]}",
        }]
      end

      PREFIXED_DIGEST = ::OpenSSL::Digest.new('SHA256').tap do |d|
        d << 'mongoid::>'
      end
      private_constant :PREFIXED_DIGEST
    end
  end
end
