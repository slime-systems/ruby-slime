# frozen_string_literal: true

module RubySlime
  module Mongoid
    module QuerySharding
      class InvalidShard < ::ArgumentError
      end

      extend ::ActiveSupport::Concern

      included do
        lambda do |hex_string|
          Integer(hex_string, 16) & 0x7fffffff
        end.then do |apply_mask|
          proc do
            ::SecureRandom.hex(4).then(&apply_mask)
          end
        end.tap do |value_proc|
          field :_qsk, as: :query_shard_key, type: :integer, default: value_proc
        end

        proc do |shard_count, shard_number|
          raise InvalidShard unless shard_count.respond_to?(:<=)
          raise InvalidShard unless shard_number.respond_to?(:<)
          begin
            raise InvalidShard unless 0 < shard_count
            raise InvalidShard unless shard_count <= 1024
            raise InvalidShard unless 0 <= shard_number
            raise InvalidShard unless shard_number < shard_count
          rescue ::ArgumentError
            raise InvalidShard
          end
          where({
            query_shard_key: {
              '$mod': [shard_count, shard_number],
            },
          })
        end.tap do |scope_proc|
          scope :query_shard, scope_proc
        end
      end
    end
  end
end
