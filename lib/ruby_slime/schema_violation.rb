# frozen_string_literal: true

module RubySlime
  class SchemaViolation < ::StandardError
    attr_reader :errors

    def initialize(errors)
      @errors = errors
      super()
    end
  end
end
