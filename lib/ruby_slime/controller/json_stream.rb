# frozen_string_literal: true

module RubySlime
  module Controller
    module JSONStream
      def json_stream(template, fills = {})
        ::Enumerator.new do |yielder|
          lookup = fills.transform_keys(&::JSON.method(:generate))
          delimiters = lookup.keys.then(&::Regexp.method(:union))
          scanner = ::StringScanner.new(::JSON.generate(template))
          loop do
            segment = scanner.scan_until(delimiters)
            break yielder << [scanner.rest] unless scanner.matched?
            matched = scanner.matched
            # noinspection RubyMismatchedArgumentType, RubyNilAnalysis
            prefix = segment.delete_suffix(matched)
            yielder << [prefix]
            yielder << lookup[matched]
          end
        end.lazy.flat_map(&:itself)
      end
    end
  end
end
