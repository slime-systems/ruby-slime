# frozen_string_literal: true

module RubySlime
  class UnknownCommand < ::NotImplementedError
    attr_reader :command

    def initialize(command)
      @command = command
      super()
    end
  end
end
