# frozen_string_literal: true

module RubySlime
  class EventQueue < EventDispatcher
    def call(events, **metadata)
      events.each do |event|
        super(event, **metadata)
      end
    end
  end
end
