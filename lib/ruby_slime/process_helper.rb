# frozen_string_literal: true

module RubySlime
  module ProcessHelper
    extend ::ActiveSupport::Concern

    # alias for convenient access
    class UnknownCommand < ::RubySlime::UnknownCommand
    end

    def state
      @state ||= {}
    end

    def pending_events
      @pending_events ||= []
    end

    class_methods do
      attr_reader :_event_handlers

      private def on(event_class, &block)
        @_event_handlers ||= {}
        @_event_handlers[event_class] = block
      end
    end

    private def apply(*events)
      handlers = self.class._event_handlers || {}
      events.flat_map do |event|
        # allow array expansions of events, hence the flatmap
        # Array() is idempotent
        Array(event)
      end.each do |event|
        handler = handlers[event.class]
        instance_exec(event, &handler) if handler
        pending_events << event
      end
    end

    private def merge_state(default, recorded)
      default.with_indifferent_access.merge(
        recorded.presence || {},
      )
    end
  end
end
