# frozen_string_literal: true

module RubySlime
  module Dry
    StrippedString = ::Dry::Types['string'].constructor(&:strip)
  end
end
