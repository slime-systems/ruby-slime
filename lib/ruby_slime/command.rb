# frozen_string_literal: true

module RubySlime
  module Command
    extend ::ActiveSupport::Concern
    extend ::Forwardable
    include ::ActiveModel::Model
    include ::ActiveModel::Attributes

    class_methods do
      attr_reader :_applied_schema

      private

      def apply_schema(schema)
        @_applied_schema = schema
      end
    end

    def validated_attributes
      @validated_attributes__immutable_cache ||= begin
        normalized = attributes.compact
        schema = self.class._applied_schema
        return normalized unless schema
        result = schema.call(normalized)
        raise SchemaViolation.new(result.errors) unless result.success?
        result.to_h
      end
    end

    def_delegators :validated_attributes, :[], :values_at
  end
end
