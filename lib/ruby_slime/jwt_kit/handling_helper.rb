# frozen_string_literal: true

module RubySlime
  module JWTKit
    module HandlingHelper
      private

      def render_data(data_hash)
        render json: {
          data: data_hash,
        }
      end

      def render_exception(code, message = nil, metadata: nil)
        render json: {
          exception: {
            code: code,
            message: message.presence,
            **{
              metadata: metadata,
            }.compact,
          }.compact,
        }, status: :unprocessable_entity
      end
    end
  end
end
