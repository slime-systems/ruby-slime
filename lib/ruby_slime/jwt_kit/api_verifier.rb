# frozen_string_literal: true

module RubySlime
  module JWTKit
    class APIVerifier < GenericVerifier
      def initialize(
        fetch_secret:,
        handle_result:
      )
        super(
          fetch_secret: fetch_secret,
          handle_result: handle_result,
          **P_APIVariation.instance.then do |holder|
            {
              required_claims: holder.required_claims,
              payload_schema: holder.payload_schema,
            }
          end,
        )
      end

      class P_APIVariation
        include ::Singleton
        attr_reader(*%i[
          required_claims
          payload_schema
        ])

        def initialize
          @required_claims = %w[aud iat sub]
          @payload_schema = ::Dry::Schema.Params do
            required(:aud).filled(:string)
            required(:iat).value(:integer, gt?: 0)
            required(:sub).filled(:string)
            optional(:data).hash
          end
          super
          freeze
        end
      end

      private_constant :P_APIVariation
    end
  end
end
