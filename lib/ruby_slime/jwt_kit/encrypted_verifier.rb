# frozen_string_literal: true

module RubySlime
  module JWTKit
    class EncryptedVerifier
      def initialize(
        fetch_secret:,
        handle_result:,
        payload_schema:
      )
        @fetch_secret = fetch_secret
        @handle_result = handle_result
        @payload_schema = payload_schema
      end

      def call(token)
        raise ::JWT::DecodeError, 'Invalid token format!' unless token.is_a?(::String)
        header = P_GenericCache.instance.header_extract.match(token)
        raise ::JWT::DecodeError, 'Invalid token format!' unless header
        header = header[1].then do |encoded|
          ::Base64.urlsafe_decode64(encoded)
        rescue ::ArgumentError
          raise ::JWT::DecodeError, 'Invalid token format!'
        end.then do |json_string|
          ::JSON.parse(json_string, symbolize_names: true)
        rescue ::TypeError, ::JSON::JSONError
          raise ::JWT::DecodeError, 'Invalid token format!'
        end
        checked_header = check_jwt_section(header, P_GenericCache.instance.header_schema)
        secret = @fetch_secret.call(checked_header)
        begin
          ::JWE.decrypt(token, secret)
        rescue ::ArgumentError
          # (invalid base64: jwe (0.4.0) lib/jwe/base64.rb:14:in `jwe_decode')
          raise ::JWT::DecodeError, 'Invalid token format!'
        end.then do |encoded|
          ::JSON.parse(encoded, symbolize_names: true)
        rescue ::TypeError, ::JSON::JSONError
          raise ::JWT::DecodeError, 'Invalid payload format!'
        end.then do |payload|
          check_jwt_section(payload, @payload_schema)
        end.then do |payload|
          @handle_result.call(payload, header)
        end
      end

      private

      def check_jwt_section(section, schema)
        section_result = schema.call(section)
        raise SchemaViolation.new(section_result.errors) unless section_result.success?
        section_result.to_h
      end

      class P_GenericCache
        include ::Singleton
        attr_reader(*%i[
          header_extract
          header_schema
        ])

        def initialize
          @header_extract = /\A([-_0-9A-Za-z]+)\./
          @header_schema = ::Dry::Schema.Params do
            optional(:typ).filled(:string).value(eql?: 'JWT')
            optional(:zip).filled(:string)
            required(:alg).filled(:string)
            required(:enc).filled(:string)
            required(:kid).filled(:string)
          end
          super
          freeze
        end
      end

      private_constant :P_GenericCache
    end
  end
end
