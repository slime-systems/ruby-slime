# frozen_string_literal: true

module RubySlime
  module JWTKit
    class GenericVerifier
      def initialize(
        fetch_secret:,
        handle_result:,
        required_claims:,
        header_schema: nil,
        payload_schema:,
        decode_options: nil,
        with_context: false
      )
        @with_context = with_context
        @fetch_secret = fetch_secret
        @handle_result = handle_result
        @required_claims = required_claims
        @header_schema = header_schema || P_GenericCache.instance.header_schema
        @payload_schema = payload_schema
        @decode_options = (decode_options || P_GenericCache.instance.header_schema).merge({
          required_claims: @required_claims,
        }.compact)
      end

      def call(token, context: nil)
        validated_cache = {}
        kwargs = @with_context ? { context: context } : {}
        ::JWT.decode(token, nil, true, @decode_options) do |header_section|
          @fetch_secret.call(
            validated_cache[:header] ||= check_jwt_section(header_section, @header_schema),
            **kwargs,
          )
        end.then do |(payload_section, header_section)|
          [
            validated_cache[:header] ||= check_jwt_section(header_section, @header_schema),
            validated_cache[:payload] ||= check_jwt_section(payload_section, @payload_schema),
          ].reverse
        end.then do |sections|
          @handle_result.call(*sections, **kwargs)
        end
      end

      private

      def check_jwt_section(section, schema)
        section_result = schema.call(section)
        raise SchemaViolation.new(section_result.errors) unless section_result.success?
        section_result.to_h
      end

      class P_GenericCache
        include ::Singleton
        attr_reader(*%i[
          header_schema
          decode_options
        ])

        def initialize
          @header_schema = ::Dry::Schema.Params do
            optional(:typ).filled(:string).value(eql?: 'JWT')
            required(:alg).filled(:string).value(eql?: 'HS256')
            required(:kid).filled(:string)
          end
          @decode_options = {
            algorithm: 'HS256',
          }
          super
          freeze
        end
      end

      private_constant :P_GenericCache
    end
  end
end
