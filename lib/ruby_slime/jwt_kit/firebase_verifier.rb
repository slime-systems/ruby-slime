# frozen_string_literal: true

require 'typhoeus'

module RubySlime
  module JWTKit
    class FirebaseVerifier
      class TokenDataError < ::StandardError
      end

      class KeyDistributionFailed < ::StandardError
      end

      class UnknownKey < ::StandardError
      end

      def initialize(aud:, iss:, current_time: ::Time.method(:current))
        @current_time = current_time
        @verifier = GenericVerifier.new(
          header_schema: P_FirebaseCache.instance.header_schema,
          payload_schema: P_FirebaseCache.instance.payload_schema,
          decode_options: {
            algorithm: 'RS256',
            verify_expiration: true, exp_leeway: 30,
            verify_aud: true, aud: aud,
            verify_iss: true, iss: iss
          },
          required_claims: %w[aud iat iss sub],
          with_context: true,
          fetch_secret: method(:fetch_firebase_secret),
          handle_result: method(:finalize_firebase_result),
        )
      end

      def call(token)
        @verifier.call(token, context: {
          initiate_at: @current_time.call,
        })
      end

      private

      def fetch_firebase_secret(header, context:)
        P_KeyCache.instance.fetch(header[:kid], context: context)
      end

      def finalize_firebase_result(*trusted, **_)
        trusted.tap do |(payload, _header)|
          auth_time, iat = payload.values_at(:auth_time, :iat).map do |timestamp|
            ::Time.at(timestamp)
          end
          with_leeway = @current_time.call + 30.seconds
          raise TokenDataError unless auth_time < with_leeway
          # https://github.com/jwt/ruby-jwt/issues/273
          # https://firebase.google.com/docs/auth/admin/verify-id-tokens#verify_id_tokens_using_a_third-party_jwt_library
          raise TokenDataError unless iat < with_leeway
        end
      end

      class P_FirebaseCache
        include ::Singleton
        attr_reader(*%i[
          header_schema
          payload_schema
        ])

        def initialize
          @header_schema = ::Dry::Schema.Params do
            required(:alg).filled(:string).value(eql?: 'RS256')
            required(:kid).filled(:string)
          end
          @payload_schema = ::Dry::Schema.Params do
            required(:exp).value(:integer, gt?: 0)
            required(:iat).value(:integer, gt?: 0)
            required(:aud).filled(:string)
            required(:iss).filled(:string)
            required(:sub).filled(:string)
            required(:auth_time).value(:integer, gt?: 0)
            optional(:picture).maybe(:string)
            optional(:email).maybe(:string)
            optional(:email_verified).value(:bool)
            optional(:firebase).maybe(:hash)
          end
          super
          freeze
        end
      end

      class P_KeyCache
        include ::Singleton
        KEY_DISTRIBUTION_URL = 'https://www.googleapis.com/robot/v1/metadata/x509/securetoken@system.gserviceaccount.com'
        private_constant(:KEY_DISTRIBUTION_URL)

        def initialize
          super
          @latest_fetch = nil
          @parse_cache = {}
          @exp = nil
          @mutex = ::Mutex.new
        end

        def fetch(kid, context:)
          @parse_cache.fetch(kid) do |cache_key|
            @parse_cache[cache_key] = key_map(context: context)[cache_key].then do |raw_key|
              raise UnknownKey unless raw_key.present?
              ::OpenSSL::X509::Certificate.new(raw_key).public_key
            end
          end
        end

        private

        def key_map(context:)
          current_time = context[:initiate_at]
          cut_off = current_time - 5.minutes
          return @latest_fetch unless !@latest_fetch || @exp.try(:<, cut_off)
          @mutex.synchronize do
            return @latest_fetch unless !@latest_fetch || @exp.try(:<, cut_off)
            10.times do |try_count|
              raise KeyDistributionFailed if 3 <= try_count
              r = ::Typhoeus.get(
                KEY_DISTRIBUTION_URL,
                followlocation: true,
                connecttimeout: 2,
              )
              break r if r.success?
            end.then do |r|
              [::JSON.parse(r.body), determine_expiry(r.headers, current_time)]
            rescue ::JSON::JSONError, ::TypeError, ::ArgumentError
              raise KeyDistributionFailed
            end.tap do |(parsed, exp)|
              raise KeyDistributionFailed unless parsed.respond_to?(:[])
              raise KeyDistributionFailed unless parsed.respond_to?(:fetch)
              @latest_fetch = parsed
              @exp = exp
              @parse_cache = {}
            end.then(&:first)
          end
        end

        def determine_expiry(headers, fetch_time)
          cache_control, age = headers.values_at('cache-control', 'age')
          raise KeyDistributionFailed unless cache_control.respond_to?(:split)
          max_age = cache_control.split(/\s*,\s*/).each do |part|
            matched = /max-age=(\d+)/.match(part)
            next unless matched
            break Integer(matched[1])
          end
          raise KeyDistributionFailed unless max_age.try(:>, 0)
          age = age.then do |string|
            Integer(string)
          rescue ::TypeError, ::ArgumentError
            0
          end
          fetch_time + (max_age - age).seconds
        end
      end

      private_constant :P_FirebaseCache, :P_KeyCache
    end
  end
end
