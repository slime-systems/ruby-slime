# frozen_string_literal: true

module RubySlime
  module JWTKit
    class SchemaViolation < ::RubySlime::SchemaViolation
    end
  end
end
