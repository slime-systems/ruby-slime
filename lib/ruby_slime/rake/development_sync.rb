# frozen_string_literal: true

module RubySlime
  module Rake
    class DevelopmentSync
      include Context

      def initialize(
        rake_context:,
        path_prefix: '',
        path_namespace:,
        rclone_conf: 'sync.rclone.conf',
        rclone_remote: 'sync',
        process_paths: lambda(&:itself),
        chdir: nil
      )
        initialize_context(rake_context)
        @rclone_conf = rclone_conf
        @rclone_remote = rclone_remote
        @path_namespace = [path_prefix, path_namespace].select(&:present?).then do |path_sections|
          ::File.join(*path_sections)
        end
        @check_rclone_conf = lambda do
          ::File.expand_path(@rclone_conf).tap do |full_path|
            raise "\"#{@rclone_conf}\" not found!" unless ::File.file?(full_path)
          end
        end
        @process_paths = process_paths
        @chdir = chdir
      end

      def declare
        desc('rclone sync up')
        task('sync-up') { sync_up('copy') }

        desc('rclone sync up preview')
        task('sync-up-preview') { sync_up('copy', echo: true) }

        desc('rclone sync up but also delete files on remote to match local')
        task('sync-up-unsafe') { sync_up('sync') }

        desc('rclone sync down')
        task('sync-down', &method(:sync_down))
      end

      private

      def sync_up(rclone_command, echo: false)
        include_options = ensure_chdir do
          `git ls-files -io --exclude-per-directory=.gitignore`
            .each_line
            .map(&:strip)
            .select(&:present?)
            .then(&@process_paths)
            .select(&::File.method(:file?))
            .map { |file_path| "--include=#{file_path}" }
        end
        raise 'nothing to sync' unless include_options.present?
        config_path = @check_rclone_conf.call
        ensure_chdir do
          sh(
            *if echo
              %w[echo rclone]
            else
              %w[rclone]
            end,
            rclone_command,
            "--config=#{config_path}",
            *include_options,
            '.',
            "#{@rclone_remote}:#{@path_namespace}/",
          )
        end
      end

      def sync_down(*_args, **_kwargs)
        config_path = @check_rclone_conf.call
        ensure_chdir do
          sh(
            'rclone',
            'copy',
            "--config=#{config_path}",
            "#{@rclone_remote}:#{@path_namespace}/",
            '.',
          )
        end
      end

      def ensure_chdir(&shell)
        if @chdir.present?
          ::Dir.chdir(@chdir, &shell)
        else
          shell.call
        end
      end
    end
  end
end
