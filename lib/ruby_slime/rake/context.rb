# frozen_string_literal: true

module RubySlime
  module Rake
    module Context
      def initialize_context(context)
        @_rake_context = context
      end

      private

      def desc(*args, **kwargs, &block)
        @_rake_context.call(:desc, *args, **kwargs, &block)
      end

      def task(*args, **kwargs, &block)
        @_rake_context.call(:task, *args, **kwargs, &block)
      end

      def file(*args, **kwargs, &block)
        @_rake_context.call(:file, *args, **kwargs, &block)
      end

      def sh(*args, **kwargs, &block)
        @_rake_context.call(:sh, *args, **kwargs, &block)
      end
    end
  end
end
