# frozen_string_literal: true

module RubySlime
  class EventDispatcher
    include Internals::EventMetadataHandler

    def initialize(*handlers)
      @handlers = handlers
      freeze
    end

    def call(events, **metadata)
      @handlers.each do |handler|
        handle_handler_arguments(handler, events, metadata)
      end
    end
  end
end
