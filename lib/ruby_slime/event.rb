# frozen_string_literal: true

module RubySlime
  module Event
    extend ::ActiveSupport::Concern

    attr_reader :data, :metadata

    def initialize(data: {}, metadata: nil)
      @data = data
      @metadata = metadata
    end

    class_methods do
      def strict(data: {}, metadata: nil)
        new(
          data: _ensure_schema(data),
          metadata: metadata,
        )
      end

      private

      def apply_schema(schema)
        @_applied_schema = schema
      end

      def _ensure_schema(data)
        return data unless @_applied_schema
        result = @_applied_schema.call(data)
        raise SchemaViolation.new(result.errors) unless result.success?
        result.to_h
      end
    end
  end
end
