# frozen_string_literal: true

module RubySlime
  module ObjectSharing
    extend ::ActiveSupport::Concern

    class_methods do
      private def share_objects(objects_hash)
        _carrier = (@_object_sharing_carrier ||= {}).tap do |base|
          base.merge!(objects_hash)
        end
        if !method_defined?(:shared_objects)
          define_method(:shared_objects) do
            _carrier
          end
        else
          define_method(:shared_objects) do
            @_shared_objects ||= {
              **super,
              **_carrier,
            }
          end
        end
      end
    end
  end
end
