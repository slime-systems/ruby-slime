# frozen_string_literal: true

module RubySlime
  module Internals
    module EventMetadataHandler
      include ObjectSharing

      private

      def handle_handler_arguments(handler, events, metadata)
        handler.call(events, **metadata)
      rescue ::ArgumentError => e
        should_retry = shared_objects[:meta_patterns].any? do |pattern|
          pattern =~ e.message
        end
        raise e unless should_retry
        handler.call(events)
      end

      share_objects({
        meta_patterns: [
          /wrong\s+number\s+of\s+arguments/i,
          /unknown\s+keywords/i,
        ],
      })
    end
  end
end
