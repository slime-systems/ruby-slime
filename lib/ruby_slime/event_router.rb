# frozen_string_literal: true

module RubySlime
  class EventRouter
    include Internals::EventMetadataHandler

    def initialize(handlers_map = {})
      @handlers_map = handlers_map
      freeze
    end

    def call(event, **metadata)
      handler = @handlers_map[event.class]
      return unless handler
      handle_handler_arguments(handler, event, metadata)
    end
  end
end
